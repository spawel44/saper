#include "saper.h"

int main(void)
{
	char c = 0;
        int unrevealed;
	long int time1, time2;

        struct game_sets sets;
        struct brd **pbrd;
        struct pos pos;

        while (menu()) {
		menu_sets(&sets);
		pbrd = brd_set(&sets.size);
		brd_disp(sets.size, pbrd);
                if (first_click(&pos, &sets, pbrd, &unrevealed) == QUIT) {
			brd_free(sets.size, &pbrd);
			continue;
		}
		time1 = get_time();
                while (unrevealed) {
			brd_disp(sets.size, pbrd);
                        c = click(&pos, sets.size, pbrd, &unrevealed);
                        if (c == QUIT || c == MINE)
				break;
                }
		if (c == QUIT) {
			brd_free(sets.size, &pbrd);
			continue;
		}
		time2 = get_time() - time1;
                reveal_all(pbrd, sets.size);
                brd_disp(sets.size, pbrd);
                end_dial(unrevealed, time2);

		brd_free(sets.size, &pbrd);
		continue;
        }
	puts("Goodbye!");
	return 0;
}

static void set_flag(struct pos *pos, struct brd *pbrd[])
{
	if (!pbrd[pos->x][pos->y].flag &&
		!pbrd[pos->x][pos->y].revealed)
			pbrd[pos->x][pos->y].flag = true;
	else
		pbrd[pos->x][pos->y].flag = false;
}

static int click(struct pos *pos, int size, struct brd *pbrd[], int *unrevealed)
{
	char c;

	c = get_pos(pos, size);

	if (c == QUIT)
		return QUIT;
        
        if (c == FLAG) {
        	set_flag(pos, pbrd);
        	return 0;
        }
	if (hit_mine(pos, pbrd))
                return MINE;

        if (!pbrd[pos->x][pos->y].revealed && is_mine_num(pos, pbrd)) {
                pbrd[pos->x][pos->y].revealed = true;
                --(*unrevealed);
        } else {
                *unrevealed -= reveal_empty(pbrd, size, pos->x, pos->y);
        }
        return 0;
}

static int first_click(struct pos *pos, struct game_sets *sets, struct brd *pbrd[], int *unrevealed)
{
        int mines;
        char c;

        c = get_pos(pos, sets->size);

	if (c == QUIT)
		return QUIT;

        mines = brd_fill(sets, pos, pbrd);
        *unrevealed = cnt_empty(mines, sets->size);
        
        if (c == FLAG) {
        	set_flag(pos, pbrd);
        	return 0;
        }
        if (is_mine_num(pos, pbrd)) {
                pbrd[pos->x][pos->y].revealed = true;
                --(*unrevealed);
        } else {
                *unrevealed -= reveal_empty(pbrd, sets->size, pos->x, pos->y);
        }
	return 0;
}

static bool hit_mine(struct pos *pos, struct brd *pbrd[])
{
        return pbrd[pos->x][pos->y].content == MINE ? true : false;
}

static bool is_mine_num(struct pos *pos, struct brd *pbrd[])
{
        if (pbrd[pos->x][pos->y].content > 0 && pbrd[pos->x][pos->y].content < 9)
                return true;
        return false;
}

static int get_pos(struct pos *pos, int size)
{
        int n;
        char c;
        
	do {
		puts("Enter number: ROW,COL[f] - flag or 'q' for menu:");
		c = getchar();
		if (tolower(c) == QUIT)
			return QUIT;
		else
			ungetc(c, stdin);

		n = scanf("%d,%d", &pos->x, &pos->y);
		if ((c = getchar()) != '\n')
			while(getchar() != '\n');
	
	} while ((pos->x < 0 || pos->x > size - 1 ||
                pos->y < 0 || pos->y > size - 1) || n != 2 ||
                (tolower(c) != FLAG && c != '\n'));

        return tolower(c);
}

static int cnt_empty(int mines, int size)
{
	return size * size - mines;
}

static void reveal_all(struct brd *pbrd[], int size)
{
        for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
                	pbrd[i][j].revealed = true;
                	pbrd[i][j].flag = false;
        	}
	}
}

static int reveal_empty(struct brd *pbrd[], int size, int x, int y)
{
        int revealed = 0;
	if (!pbrd[x][y].revealed && pbrd[x][y].content == EMPTY) {
		pbrd[x][y].revealed = true;
		++revealed;

		if (x - 1 >= 0) {
			revealed += reveal_empty(pbrd, size, x - 1, y);
		}
		if (x + 1 < size) {
			revealed += reveal_empty(pbrd, size, x + 1, y);
		}
		if (y - 1 >= 0) {
			revealed += reveal_empty(pbrd, size, x, y - 1);
		}
		if (y + 1 < size) {
			revealed += reveal_empty(pbrd, size, x, y + 1);
		}
	}
	return revealed;
}

static int brd_fill(struct game_sets *sets, struct pos *pos, struct brd *pbrd[])
{
	int mines = 0;
	switch (sets->difficulty) {
		case 1:
			mines = sets->size * sets->size * 15 / 100;
			break;
		case 2:
			mines = sets->size * sets->size * 25 / 100;
			break;
		case 3:
			mines = sets->size * sets->size * 35 / 100;
			break;
	}

	time_t t;
	srand((unsigned) time(&t));

	int mine_x, mine_y;
	for (int i = 0; i < mines; ++i) {
                /* repeat if the mine was drawn on the first 'click' */
		do {
			mine_x = rand() % (sets->size - 1);
			mine_y = rand() % (sets->size - 1);
		} while (mine_x == pos->x && mine_y == pos->y &&
                         pbrd[mine_x][mine_y].content == MINE);

		pbrd[mine_x][mine_y].content = MINE;
	}

	for (int i = 0; i < sets->size; ++i)
	for (int j = 0; j < sets->size; ++j)
		if (pbrd[i][j].content == MINE)
			for (int n = -1; n < 2; ++n)
			for (int m = -1; m < 2; ++m)
				if ((i + n >= 0 && i + n < sets->size &&
                                j + m >= 0 && j + m < sets->size) &&
				pbrd[i + n][j + m].content != MINE)
					++pbrd[i + n][j + m].content;
        return mines;
}

static void brd_disp(int size, struct brd *pbrd[])
{
	system("clear");
	putchar(' ');
	putchar(' ');

	int i = 0;
	/* alignment */
	while (i < size) {
		if (size > 10 && i <= 10)
			putchar(' ');
		printf(" %d", i++);
	}
	putchar('\n');

	for (i = 0; i < size; ++i) {
		printf("\n%d  ", i);
		if (size > 10 && i < 10)
			putchar(' ');

		for (int j = 0; j < size; ++j) {
			if (pbrd[i][j].flag) {
				printf("f ");
				if (size > 10)
					putchar(' ');
				continue;
			}
			if (pbrd[i][j].revealed) {
				if (pbrd[i][j].content > 0 && pbrd[i][j].content < 9) {
					printf("%d ", pbrd[i][j].content);
					if (size > 10)
						putchar(' ');
				} else if (pbrd[i][j].content == MINE) {
				        printf("M ");
					if (size > 10)
						putchar(' ');
				} else {
                                	printf("O ");
					if (size > 10)
						putchar(' ');
                        	}
				continue;
			} else {
				printf("X ");
				if (size > 10)
					putchar(' ');
			}
		}
	}
	putchar('\n');
	putchar('\n');
}

static struct brd **brd_set(int *size)
{
	switch (*size) {
		case 1:
			*size = 10;
			break;
		case 2:
			*size = 20;
			break;
		case 3:
			*size = 30;
			break;
	}

	struct brd **pbrd;

	if ((pbrd = calloc(*size, sizeof(struct brd *))) == NULL)
                exit(EXIT_FAILURE);
	for (int i = 0; i < *size; ++i)
		if ((pbrd[i] = calloc(*size, sizeof(struct brd))) == NULL)
			exit(EXIT_FAILURE);
	return pbrd;
}

static void brd_free(int size, struct brd **pbrd[])
{
	for (int i = 0; i < size; ++i)
		free(pbrd[0][i]);
	free(pbrd[0]);
}

static void end_dial(int unrevealed, long int time2)
{
        if (!unrevealed)
                puts("Congratulations! You won!\n");
        else
                puts("Oops, you hit a mine.. :(\n");

	printf("Game time: %ld seconds.\n\n", time2);
	puts("Press enter..");
	getchar();
}

static void sets_get(int *sets)
{
	while ((scanf("%d", sets) != 1) || *sets < 1 || *sets > 3) {
		while (getchar() != '\n');
		puts("Number from 1 to 3:");
	}
	while (getchar() != '\n');
}

static void menu_sets(struct game_sets *sets)
{
	putchar('\n');
	puts("Difficulty (1 - 3):");
	sets_get(&sets->difficulty);
	putchar('\n');

	puts("Board size (1 - 3):");
	sets_get(&sets->size);
}

static int menu(void)
{
	system("clear");
	puts("Welcome to Saper!");
	putchar('\n');
	puts("(n) new game.");
	puts("(q) quit");
	putchar('\n');

	char c;
	while ((c = getchar())) {
                if (c == '\n') {
                        continue;
                }
		if (c != 'n' && c != 'q') {
			system("clear");
			putchar('\n');
			puts("(n) new game.");
			puts("(q) quit.");
			putchar('\n');
			while (getchar() != '\n');
			continue;
		}
		if (c == 'n') {
			while (getchar() != '\n');
			return 1;
		} else {
			while (getchar() != '\n');
			return 0;
		}
	}
	return 0;
}

static long int get_time(void)
{
	time_t seconds;
	return time(&seconds);;
}
