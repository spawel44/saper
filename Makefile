CFLAGS = -Wall -Wextra -O2
CC = gcc
saper: saper.c saper.h
	$(CC) saper.c $(CFLAGS) -o saper

clean:
	-rm -f saper
