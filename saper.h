#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define QUIT 'q'
#define WON 0
#define FLAG 'f'

#define EMPTY 0
#define MINE 9

struct game_sets {
	int difficulty;
	int size;
};

struct brd {
	bool revealed;
	bool flag;
	int content;
};

struct pos {
	int x;
	int y;
};

void game(void);

static void set_flag(struct pos *pos, struct brd *pbrd[]);
static int click(struct pos *pos, int size, struct brd *pbrd[], int *empty);
static int first_click(struct pos *pos, struct game_sets *sets, struct brd *pbrd[], int *empty);
static bool hit_mine(struct pos *pos, struct brd *pbrd[]);
static bool is_mine_num(struct pos *pos, struct brd *pbrd[]);
static int get_pos(struct pos *pos, int size);
static int cnt_empty(int mines, int size);
static void reveal_all(struct brd *pbrd[], int size);
static int reveal_empty(struct brd *pbrd[], int size, int x, int y);

static int brd_fill(struct game_sets *sets, struct pos *pos, struct brd *pbrd[]);
static void brd_disp(int size, struct brd *pbrd[]);
static struct brd **brd_set(int *size);
static void brd_free(int size, struct brd **pbrd[]);

static void end_dial(int empty, long int time2);
static void sets_get(int *sets);
static void menu_sets(struct game_sets *sets);
static int menu(void);
static long int get_time(void);
